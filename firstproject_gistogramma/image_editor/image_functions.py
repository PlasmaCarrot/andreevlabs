# This Python file uses the following encoding: utf-8
import os,sys
import PyQt5
import math
from math import fabs
import numpy as np
import numpy
import matplotlib.pyplot as plt
#%matplotlib inline
from PyQt5.QtWidgets import QDoubleSpinBox, QSpinBox
from PyQt5.QtGui import QImage, QColor

def hist(img: QImage):
    ret = [[],[],[]]
    for m in ret:
        for i in range(256):
            m.append(0)
    for i in range(img.width()):
        for j in range(img.height()):
            color = img.pixelColor(i,j)
            ret[0][color.red()] += 1
            ret[1][color.green()] += 1
            ret[2][color.blue()] += 1

    return ret

def make_halftone(img: QImage):
    for x in range(img.width()):
        for y in range(img.height()):
            color = img.pixelColor(x, y)
            new_color = int((color.red() + color.green() + color.blue()) / 3)
            qColor = QColor(new_color, new_color, new_color)
            img.setPixelColor(x, y, qColor)
    return img

def make_diff(img1: QImage, img2: QImage):
    if img1.width() == img2.width() and img1.height() == img2.height():
        for x in range(img1.width()):
            for y in range(img1.height()):
                color_1 = img1.pixelColor(x, y)
                color_2 = img2.pixelColor(x, y)
                d_r = color_1.red() - color_2.red() + 127
                #if d_r < 0 :
                #    d_r = 0
                d_g = color_1.green() - color_2.green() + 127
                #if d_g < 0:
                #    d_g = 0
                d_b = color_1.blue() - color_2.blue() + 127
                #if d_b < 0:
                #    d_b = 0

                new_color = QColor(d_r, d_g, d_b)
                img1.setPixelColor(x, y, new_color)
    
    return img1

def make_sum(img1: QImage, img2: QImage):
    if img1.width() == img2.width() and img1.height() == img2.height():
        for x in range(img1.width()):
            for y in range(img1.height()):
                color_1 = img1.pixelColor(x, y)
                color_2 = img2.pixelColor(x, y)
                d_r = color_1.red() + color_2.red()
                if d_r > 255:
                    d_r = 255
                d_g = color_1.green() + color_2.green()
                if d_g > 255:
                    d_g = 255
                d_b = color_1.blue() + color_2.blue()
                if d_b > 255:
                    d_b = 255

                new_color = QColor(d_r, d_g, d_b)
                img1.setPixelColor(x, y, new_color)

    return img1

def make_mult(img1: QImage, Mult_SpinBox: float, is_diff=None):
    offset = 0 if is_diff is None else 127
    for x in range(img1.width()):
        for y in range(img1.height()):
            color_1 = img1.pixelColor(x, y)
            d_r = (color_1.red()-offset)*Mult_SpinBox+offset
            if d_r > 255:
                d_r = 255
            d_g = (color_1.green()-offset)*Mult_SpinBox+offset
            if d_g > 255:
                d_g = 255
            d_b = (color_1.blue()-offset)*Mult_SpinBox+offset
            if d_b > 255:
                d_b = 255

            new_color = QColor(d_r, d_g, d_b)
            img1.setPixelColor(x, y, new_color)

    return img1

def make_bin(img1: QImage, threshold_SpinBox: float):
    for x in range(img1.width()):
        for y in range(img1.height()):
            color_1 = img1.pixelColor(x, y)
            d_r = color_1.red()
            if d_r > threshold_SpinBox:
                d_r = 255
            else :
                d_r = 0
            d_g = color_1.green()
            if d_g > threshold_SpinBox:
                d_g = 255
            else :
                d_g = 0
            d_b = color_1.blue()
            if d_b > threshold_SpinBox:
                d_b = 255
            else :
                d_b = 0

            new_color = QColor(d_r, d_g, d_b)
            img1.setPixelColor(x, y, new_color)

    return img1


def make_sobel(img1: QImage):
    img=QImage(img1)
    Hx=[[-1,0,1],
        [-2,0,2],
        [-1,0,1]]
    Hy=[[-1,-2,-1],
        [0,0,0],
        [1,2,1]]
    for x in range(1,img1.height()-1):
        for y in range(1,img1.width()-1):
            new_x=0
            new_y=0
            for hw in [-1,0,1]:
                for wi in [-1,0,1]:
                    new_color = int((img1.pixelColor(y+wi, x+hw).red()+ img1.pixelColor(y+wi, x+hw).green()+img1.pixelColor(y+wi, x+hw).blue())/ 3)
                    new_x+=Hx[hw+1][wi+1]*new_color
                    new_y+=Hy[hw+1][wi+1]*new_color
            new_color1=int(math.sqrt(new_x**2+new_y**2))
            if new_color1>255:
                new_color1=255
            if new_color1<0:
                new_color1=0

            qColor = QColor(new_color1, new_color1, new_color1)
            img.setPixelColor(y, x, qColor)
    return img

def make_prewitt(img1: QImage):
    img=QImage(img1)
    Hx=[[1,1,1],
        [0,0,0],
        [-1,-1,-1]]
    Hy=[[-1,0,1],
        [-1,0,1],
        [-1,0,1]]
    for x in range(1,img1.height()-1):
        for y in range(1,img1.width()-1):
            new_x=0
            new_y=0
            for hw in [-1,0,1]:
                for wi in [-1,0,1]:
                    new_color = int((img1.pixelColor(y+wi, x+hw).red()+ img1.pixelColor(y+wi, x+hw).green()+img1.pixelColor(y+wi, x+hw).blue())/ 3)
                    new_x+=Hx[hw+1][wi+1]*new_color
                    new_y+=Hy[hw+1][wi+1]*new_color
            new_color1=int(math.sqrt(new_x**2+new_y**2))
            if new_color1>255:
                new_color1=255
            if new_color1<0:
                new_color1=0

            qColor = QColor(new_color1, new_color1, new_color1)
            img.setPixelColor(y, x, qColor)
    return img

def make_move(img1: QImage, dX:int, dY:int):
    img=QImage(img1)
    for x in range(1,img1.height()-1):
        for y in range(1,img1.width()-1):
            img.setPixelColor(y,x,QColor(0,0,0))
    if (dY  >=0): #Смещение вниз
       for x in range(1,img1.height()-dY):
           if (dX>=0): #Смещение вниз/вправо
                for y in range(1,img1.width()-dX):
                    img.setPixelColor(y+dX, x+dY, img1.pixelColor(y,x))
           else: #вниз/влево
                for y in range(1,img1.width()+dX):
                    img.setPixelColor(y, x+dY, img1.pixelColor(y-dX,x))
    else: #смещение вверх
        for x in range(1,img1.height()+dY):
            if (dX>=0): #Смещение вверх/вправо
                for y in range(1,img1.width()-dX):
                    img.setPixelColor(y+dX, x, img1.pixelColor(y,x-dY))
            else: #Смещение вверх/влево
                for y in range(1,img1.width()+dX):
                    img.setPixelColor(y,x,img1.pixelColor(y-dX,x-dY))
    return img

def make_laplas(img1: QImage):
    img=QImage(img1)
    mask=[[0,-1,0],
        [-1,4,-1],
        [0,-1,0]]
    R=0
    G=0
    B=0
    H=img1.height()
    W=img1.width()
    matrix=numpy.zeros([H,W])
    max_color_value=0
    for x in range(1,img1.height()-1):
        for y in range(1,img1.width()-1):
            new_color_value=0
            for hw in [-1,0,1]:
                for wi in [-1,0,1]:
                    new_color = int((img1.pixelColor(y+wi, x+hw).red()+ img1.pixelColor(y+wi, x+hw).green()+img1.pixelColor(y+wi, x+hw).blue())/ 3)
                    new_color_value+=mask[hw+1][wi+1]*new_color

            if (math.fabs(new_color_value)>max_color_value):
                max_color_value=math.fabs(new_color_value)
            matrix[x][y]=new_color_value
    for x in range(1,img1.height()-1):
        for y in range(1,img1.width()-1):
            R=matrix[x][y]/max_color_value*127+128
            G=matrix[x][y]/max_color_value*127+128
            B=matrix[x][y]/max_color_value*127+128
            qColor=QColor(R,G,B)
            img.setPixelColor(y,x,qColor)
    return img

def load_image(file_name):
    img = QImage()
    if not img.load(file_name):
        raise FileNotFoundError(file_name)
    return img
