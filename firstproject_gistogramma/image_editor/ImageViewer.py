# This Python file uses the following encoding: utf-8
import sys, os
import numpy as np
from PyQt5 import QtCore
from PyQt5 import QtGui, uic
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QDoubleSpinBox
import matplotlib.pyplot as plt
from .image_functions import load_image, make_halftone, make_diff, hist, make_sum, make_mult
from .image_functions import make_bin, make_sobel, make_prewitt, make_laplas,make_move

__DIRECTORY__ = os.path.dirname(os.path.realpath(__file__))
l = lambda f: os.path.join(__DIRECTORY__, f)

class ImageViewer(QtWidgets.QMainWindow):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)

        self.ui = uic.loadUi(l('main.ui'), self)

        self.scrollArea_A.setWidgetResizable(True)
        self.scrollArea_B.setWidgetResizable(True)
        self.scrollArea_C.setWidgetResizable(True)

        self.open_pic_A.clicked.connect(lambda: self.openImage("A"))
        self.open_pic_B.clicked.connect(lambda: self.openImage("B"))
        self.open_pic_C.clicked.connect(lambda: self.openImage("C"))

        self.halftone_button.clicked.connect(self.showHalftone)
        self.diff_button.clicked.connect(self.showDiff)
        self.Hist_Button.clicked.connect(self.showHist)
        self.sum_button.clicked.connect(self.showSum)
        self.mult_button.clicked.connect(self.showMult)
        self.Bin_button.clicked.connect(self.showBin)
        self.Sobel_Button.clicked.connect(self.showSobel)
        self.Prewitt_Button.clicked.connect(self.showPrewitt)
        self.Laplas_Button.clicked.connect(self.showLaplas)
        self.move_Button.clicked.connect(self.showMove)

        self.is_diff_dict = {"A":False, "B": False, "C": False}

        # Open image dialog
        self.ui.show()

    def showHist(self):
        pic_name_1 = self.operand_1_box.currentText()
        img_ = getattr(self, "pic_label_"+pic_name_1).pixmap().toImage()

        img = hist(img_)
        #np.arange(255)
        positions=np.arange(256)

        plt.subplot(3, 1, 1)
        plt.grid(True)
        plt.bar(positions,img[0],color='red')
        #plt.hist(img[0], color='red',bins=x)
        plt.title('Red Channel', fontsize=14, color='red')

        plt.subplot(3, 1, 2)
        plt.grid(True)
        plt.title("Green Channel", fontsize=14, color='green')
        plt.bar(positions,img[1], color='green')
        plt.ylabel("Number of pixels", fontsize=20, fontweight="bold")

        plt.subplot(3, 1, 3)
        plt.grid(True)
        plt.title("Blue Channel", fontsize=14, color='blue')
        plt.bar(positions,img[2], color='blue')
        plt.xlabel("Brightness", fontsize=20, fontweight="bold")

        plt.show()


    def showHalftone(self):
        pic_name_1 = self.operand_1_box.currentText()
        pic_name_2 = self.operand_3_box.currentText()
        img_ = getattr(self, "pic_label_"+pic_name_1).pixmap().toImage()
        
        # todo: need call in another thread and checking process
        img = make_halftone(img_)

        self.showImage(img, pic_name_2)
    
    def showDiff(self):
        pic_name_1 = self.operand_1_box.currentText()
        pic_name_2 = self.operand_2_box.currentText()
        pic_name_3 = self.operand_3_box.currentText()
        img_1 = getattr(self, "pic_label_"+pic_name_1).pixmap().toImage()
        img_2 = getattr(self, "pic_label_"+pic_name_2).pixmap().toImage()

        # todo: need call in another thread and checking process
        img = make_diff(img_1, img_2)
        

        self.showImage(img, pic_name_3)
        self.is_diff_dict[pic_name_3] = True

    def showSum(self):
        pic_name_1 = self.operand_1_box.currentText()
        pic_name_2 = self.operand_2_box.currentText()
        pic_name_3 = self.operand_3_box.currentText()
        img_1 = getattr(self, "pic_label_"+pic_name_1).pixmap().toImage()
        img_2 = getattr(self, "pic_label_"+pic_name_2).pixmap().toImage()

        # todo: need call in another thread and checking process
        img = make_sum(img_1, img_2)

        self.showImage(img, pic_name_3)

    def showMult(self):
        mult=self.Mult_SpinBox.value()
        pic_name_1 = self.operand_1_box.currentText()
        pic_name_2 = self.operand_3_box.currentText()
        img_ = getattr(self, "pic_label_"+pic_name_1).pixmap().toImage()

        img = make_mult(img_, mult, self.is_diff_dict[pic_name_1])

        self.showImage(img, pic_name_2)

    def showBin(self):
        threshold=self.threshold_doubleSpinBox.value()
        pic_name_1 = self.operand_1_box.currentText()
        pic_name_2 = self.operand_3_box.currentText()
        img_ = getattr(self, "pic_label_"+pic_name_1).pixmap().toImage()

        img = make_bin(img_, threshold)

        self.showImage(img, pic_name_2)

    def showSobel(self):
        pic_name_1 = self.operand_1_box.currentText()
        pic_name_2 = self.operand_3_box.currentText()
        img_ = getattr(self, "pic_label_"+pic_name_1).pixmap().toImage()
        img = make_sobel(img_)

        self.showImage(img, pic_name_2)

    def showPrewitt(self):
        pic_name_1 = self.operand_1_box.currentText()
        pic_name_2 = self.operand_3_box.currentText()
        img_ = getattr(self, "pic_label_"+pic_name_1).pixmap().toImage()
        img = make_prewitt(img_)

        self.showImage(img, pic_name_2)

    def showLaplas(self):
        pic_name_1 = self.operand_1_box.currentText()
        pic_name_2 = self.operand_3_box.currentText()
        img_ = getattr(self, "pic_label_"+pic_name_1).pixmap().toImage()
        img = make_laplas(img_)

        self.showImage(img, pic_name_2)
        self.is_diff_dict[pic_name_2] = True

    def showMove(self):
        dX=self.dX_Box.value()
        dY=self.dY_Box.value()
        pic_name_1 = self.operand_1_box.currentText()
        pic_name_2 = self.operand_3_box.currentText()
        img_ = getattr(self, "pic_label_"+pic_name_1).pixmap().toImage()

        img = make_move(img_,dX,dY)

        self.showImage(img, pic_name_2)

    def openImage(self, name):
        fname = QtWidgets.QFileDialog.getOpenFileName(
            None,'OpenFile', __DIRECTORY__,"Image file(*.png *.bmp *.jpg)"
        )[0]
        img = QtGui.QImage()
        if img.load(fname):
            self.showImage(img, name)
        else:
            print("Error!!")

    def showImage(self, img:QtGui.QImage, name):
        getattr(self,"pic_label_"+name).setGeometry(0, 0, img.width(), img.height())
        getattr(self,"scrollAreaWidgetContents_"+name).setMinimumSize(img.width(), img.height())
        getattr(self,"pic_label_"+name).setPixmap(QtGui.QPixmap().fromImage(img))

        self.is_diff_dict[name] = False
